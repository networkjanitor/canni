Canni
=====

If you are looking for the generated html code example (Triple Trouble):
https://networkjanitor.gitlab.io/canni/

If you are looking for the real, up-to-date versions, please use the following links:

* Triple Trouble: https://networkjanitor.gitlab.io/xutt/
* The Shatterer: https://networkjanitor.gitlab.io/jinni/


Technical Design Goals/Criteria
-------------------------------

* lightweight
* single page
* avoid javascript as often as possible (tabs with css, copy requires javascript :[ )
* offline portability (can save the file and open it from harddrive - still works)
* easy to use
* dataset source type as `xutt <https://gitlab.com/networkjanitor/xutt>`_ (same as the linux terminal version of Canni: `bronk <https://gitlab.com/networkjanitor/bronk>`_)

Info about changes to the dataset
---------------------------------

Sometimes when I feel like it, I'll update the used dataset here from the `xutt <https://gitlab.com/networkjanitor/xutt>`_ repo to generate a more up-to-date example.
If you have any specific change requests (regarding the dataset used for the example), please feel free to create issues (or preferably pull requests) in the `xutt <https://gitlab.com/networkjanitor/xutt>`_ repo.

TODOs
-----

* write a demo dataset instead of using xutt to demonstrate things without the triple trouble/guildwars2 terminology

